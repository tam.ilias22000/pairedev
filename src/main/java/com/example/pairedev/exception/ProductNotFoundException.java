package com.example.pairedev.exception;

public class ProductNotFoundException extends RuntimeException {

    private String name;

    public ProductNotFoundException(String name) {
        this.name = name;
    }

    public String getProductName() {
        return name;
    }
}
