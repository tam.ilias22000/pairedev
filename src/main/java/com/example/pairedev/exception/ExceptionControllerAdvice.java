package com.example.pairedev.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<HttpError> onProductNotFoundException(ProductNotFoundException pnfe) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new HttpError("Produit non trouvé : " + pnfe.getProductName()));
    }
}
