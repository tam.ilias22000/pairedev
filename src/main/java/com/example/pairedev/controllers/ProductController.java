package com.example.pairedev.controllers;

import com.example.pairedev.exception.HttpError;
import com.example.pairedev.exception.ProductNotFoundException;
import com.example.pairedev.model.Product;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductController {

    List<Product> listeDeProduits = new ArrayList<Product>(List.of(
            new Product("voiture", 3),
            new Product("fraise", 4),
            new Product("pomme", 2)));

    @GetMapping("products")
    public List<Product> getListeDeProduits() {
        return listeDeProduits;
    }

    @GetMapping("/products/{name}")
    public Product getProduct(@PathVariable String name) {
        for (Product produit : listeDeProduits) {
            if (produit.getName().equals(name)) {
                return produit;
            }
        }
        return null;
    }

    @PostMapping("/products")
    public Product postProduct(@RequestBody Product produit) {
        listeDeProduits.add(produit);
        return produit;
    }

    @PutMapping("/products/{name}")
    public ResponseEntity<Product> updateProduct(@PathVariable String name,
                                                 @RequestBody Product newProduct) {

        for (Product product : listeDeProduits) {
            if (product.getName().equals(name)) {
                product.setName(newProduct.getName());
                product.setPrice(newProduct.getPrice());
                return ResponseEntity.created(URI.create("/products/" + newProduct.getName()))
                        .body(newProduct);
            }
        }
        throw new ProductNotFoundException(name);
    }


    @DeleteMapping("/products/{name}")
    public void deleteProduct(@PathVariable String name) {
        listeDeProduits.remove(getProductByName(name));
    }

    public Product getProductByName(String name) {
        for (Product product : listeDeProduits) {
            if (product.getName().equals(name)) {
                return product;
            }
        }
        throw new ProductNotFoundException(name);
    }
}
