package com.example.pairedev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PairedevApplication {

    public static void main(String[] args) {
        SpringApplication.run(PairedevApplication.class, args);
    }

}
